export default [
    {
        "foreignLang": "alt - старый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jung - молодой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "neu - новый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "groß - большой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "riesig - огромный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "klein - маленький",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dick - толстый; густой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dünn - тонкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hungrig - голодный ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "satt - сытый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "voll - полный ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leer - пустой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gut - хороший; добрый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ausgezeichnet - отличный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erstaunlich - удивительный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wunderschön  - прекрасный, чудесный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schlecht - плохой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schrecklich - ужасный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "früh - ранний; рано",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spät - поздний; поздно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorig - последний, прошлый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nächst - следующий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "frei - свободный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kostenlos - бесплатный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beschäftigt - занятый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heiß - жаркий; горячий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "warm - тёплый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kalt - холодный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kühl - прохладный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "scharf - острый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stumpf - тупой (нож)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dumm - глупый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "klug - умный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schön - красивый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "attraktiv - привлекательный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hübsch - симпатичный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hässlich - уродливый, безобразный, отвратительный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hoch - высокий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "niedrig - низкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lang - длинный; долгий ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kurz - короткий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schwer - тяжёлый ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leicht - лёгкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schwierig - трудный, тяжелый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "einfach - простой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dunkel - тёмный ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hell - светлый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "teuer - дорогой (о цене) ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lieb - дорогой, милый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "billig - дешёвый ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "arm - бедный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "reich - богатый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gerade  - прямой; прямо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "links - cлева",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rechts - справа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "falsch - неправильный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schnell  - быстрый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "langsam - медленный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "weich - мягкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hart - твёрдый; жесткий; трудный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "traurig - печальный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "froh - радостный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "glücklich - счастливый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lustig - веселый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "böse - сердитый, злой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "höflich - вежливый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "grob - грубый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zart - нежный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mutig - смелый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "feige- трусливый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "scheu - застенчивый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fleißig - прилежный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "faul - ленивый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nützlich - полезный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nutzlos - бесполезный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stark - сильный; крепкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schwach - слабый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "laut - громкий, шумный ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leise - тихий, негромкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ruhig - тихий, спокойный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "eng  - узкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "breit - широкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sauber - чистый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schmutzig - грязный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "müde - усталый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ehrlich - честный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bequem - уютный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "seltsam - странный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "eigen - собственный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schmackhaft - вкусный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bitter - горький",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sauer - кислый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "salzig - соленый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bereit - готовый (что-то сделать)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fertig - готовый (завершенный)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "aufmerksam - внимательный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorsichtig - осторожный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Haupt... - главный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fähig - способный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "notwendig - необходимый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wichtig - важный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sicher - уверенный ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "echt - настоящий, истинный; \"правда?\"",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "besser - лучше; более хороший",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "best  - лучший",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mehr  - больше (по количеству); более",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "meist - наибольший (по количеству)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lieber - лучше, охотней",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "am liebsten - охотней всего",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "manchmal - иногда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nie - никогда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "selten - редко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gewöhnlich - обычно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oft - часто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "immer - всегда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bald  - скоро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vor kurzem - недавно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "weit - далеко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "genau - точно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wahrscheinlich - вероятно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vielleicht - может быть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wirklich - действительно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "natürlich - конечно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sicherlich - наверняка, безусловно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "offensichtlich - очевидно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "besonders - особенно ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gern - охотно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ja  - да",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nein - нет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nicht - не",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dieser - этот ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jener - тот",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "viele - много (+ слово во множественном числе)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "viel - много (+ слово в единственном числе)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wenige - мало (+ слово во множественном числе)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wenig - мало (+ слово в единственном числе)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "einige - некоторые",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mehrere - несколько",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jemand - кто-то",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "niemand - никто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "etwas - что-то",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nichts - ничто ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "alles - всё",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "alle - все",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hier - здесь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hierher - сюда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dort - там",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dorthin - туда ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "da - тут; так как",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jetzt - сейчас",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wieder - снова, опять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dann - потом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "damals - тогда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "als - чем (при сравнении); когда; в качестве",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bereits - уже",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nur - только",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "noch - ещё",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fast - почти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sehr - очень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "every - каждый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jeder - каждый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "anderer - другой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "solcher - такой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "so - так",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "also - итак",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oben - вверху",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "unten - внизу",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zusammen - вместе",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorwärts - вперед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorne - впереди",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sogar - даже",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "genug - достаточно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "auch - тоже, также",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "und - и",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oder - или",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "aber - но",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jedoch - однако",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "weil - потому что, так как ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "deshalb - поэтому",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ob - ли",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "obwohl - хотя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dass - что",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sein - быть, являться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tun - делать (вообще)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "machen - делать (что-то)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "haben - иметь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "können - мочь (быть в состоянии) ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dürfen - мочь (иметь разрешение)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wollen - хотеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mögen - любить, нравиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "möchten - хотелось бы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "müssen - быть должным (по внутренним причинам)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sollen - быть должным (по внешним причинам)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "haben zu - должен (что-то сделать)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sein zu - должен (быть сделанным)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "brauchen - нуждаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gehen - идти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fahren - ехать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "finden - находить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "halten - держать; останавливаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stehen - стоять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sitzen - сидеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hören - слышать, слушать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gewinnen - побеждать, выигрывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erhalten - получать (офиц.)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bekommen - получать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kriegen - получать (разг.)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kommen - приходить, приезжать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "werden - становиться; передаёт будущее время; передаёт страдательный залог",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "laufen - бежать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sehen - видеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schreiben - писать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fallen - падать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tragen  - носить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nehmen - брать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "geben - давать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schenken - дарить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "entschuldigen - прощать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vergessen - забывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "essen - кушать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "liegen - лежать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "legen - класть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lügen - лгать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "führen - вести",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "füttern - кормить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sagen - сказать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bezahlen - оплачивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trinken - пить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schwimmen - плавать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "singen - петь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beginnen - начинать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "klingen - звенеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rufen - звать; кричать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "anrufen - звонить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sinken - опускаться; погружаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stinken - вонять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fliegen - летать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wissen - знать ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kennen - знать, быть знакомым",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zeichnen - рисовать, чертить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "malen - рисовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "werfen - кидать, бросать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wachsen - расти",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "blasen - дуть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sprechen - разговаривать, говорить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wählen - выбирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wecken  - будить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "brechen - ломать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zeigen - показывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schneiden  - резать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "setzen, sich - садиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kosten - стоить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lassen - позволять; оставлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schließen  - закрывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schlagen - ударять, бить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lesen - читать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "senden - посылать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "verbringen - проводить (время)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leihen - одолжить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bauen - строить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fühlen - чувствовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "treffen - встречать; попасть (в цель)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schlafen - спать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "verlassen - покидать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kaufen - покупать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bringen - приносить, привозить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lehren - учить, обучать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "denken - думать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kämpfen - бороться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fangen - ловить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "befürchten - бояться, опасаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "interessieren, sich - интересоваться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "überraschen - удивлять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "aufstehen - вставать ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ziehen - тянуть; перемещаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "anziehen - надевать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ausziehen - снимать (одежду)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leben - жить ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wohnen - проживать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "arbeiten - работать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fernsehen - смотреть телевизор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "waschen - мыть, cтирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rasieren, sich - бриться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "versuchen - пытаться, пробовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "feiern - праздновать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lächeln - улыбаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lachen - смеяться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "weinen - плакать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lernen - учить(ся)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "studieren - учиться (в вузе); изучать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ändern - (из)менять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "öffnen - открывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tanzen - танцевать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fragen - спрашивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bitten - просить ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bieten - предлагать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "antworten - отвечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sammeln - собирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gefallen - нравиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kochen - варить; готовить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "backen - печь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "riechen - пахнуть; нюхать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "warten - ждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erwarten - ожидать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "danken - благодарить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "spielen - играть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rauchen - курить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "wünschen - желать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schreien - кричать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "träumen - мечтать; видеть сон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hoffen - надеяться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erinnern, sich - вспоминать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erinnern - напоминать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "genießen - наслаждаться; пользоваться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erklären - объяснять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bleiben - оставаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "erholen, sich - отдыхать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "abbiegen - повернуть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heben - поднимать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "glauben - верить, полагать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "helfen - помогать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bestellen - заказывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "befehlen - приказывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "besuchen - посещать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "prüfen  - проверять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "springen - прыгать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "reisen - путешествовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "entscheiden - решать, принимать решение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lösen - решать (проблему); растворять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stimmen - быть правильным, верным",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zustimmen - соглашаться ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "retten - спасать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "speichern - сохранять, накапливать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sparen - экономить, копить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "streiten - спорить ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zählen - считать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sorgen, sich - беспокоиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "scherzen - шутить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bewegen, sich - двигаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "passen - соответствовать, подходить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sterben - умирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beeinflussen - влиять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "unterstützen - поддерживать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beschreiben - описывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bestrafen - наказывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorhaben - намереваться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "klagen - жаловаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vermeiden - избегать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "zurückkehren - возвращаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "stören - беспокоить, мешать ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorstellen - представлять, знакомить ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "kennenlernen - узнавать, знакомиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "überzeugen - убеждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "genehmigen - позволять, санкционировать, одобрять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "schätzen - ценить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Leute -  люди",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Mensch - человек",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Mann - мужчина; муж",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Frau - женщина; жена",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Kind  - ребенок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Junge - мальчик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Mädchen - девочка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Freund - друг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Bekannte - знакомый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Nachbar - сосед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Gast  - гость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Chef  - начальник; шеф",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Konkurrent - конкурент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kunde - клиент, покупатель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kollege - коллега",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Familie - семья",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Eltern  - родители",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Vater - отец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Vati - папа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Mutter - мать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Mutti - мама",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Sohn - сын",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Tochter - дочь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Bruder - брат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Schwester - сестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Großvater - дед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Opa - дедушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Großmutter - бабушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Oma - бабуля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Enkel - внук",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Enkelin - внучка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schwiegervater - тесть, свекор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Schwiegermutter - тёща, свекровь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Onkel - дядя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Tante - тетя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Cousin [кузэн] - двоюродный брат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Kusine - двоюродная сестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Neffe - племянник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Nichte - племянница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Arbeit - работа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Geschäftsmann - бизнесмен",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Lehrer - учитель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Fahrer  - водитель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Arbeiter  - рабочий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Ingenieur - инженер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Arzt - врач",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Rechtsanwalt - адвокат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Journalist - журналист",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Krankenschwester  - медсестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Verkäufer - продавец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kellner - официант",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Buchhalter - бухгалтер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Maler - художник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Musiker - музыкант",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schauspieler - актер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Student - студент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schüler - школьник, ученик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Tier - животное",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Katze - кошка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Hund - собака",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Vogel - птица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Eichhörnchen - белка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Wolf - волк",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Gans - гусь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Giraffe - жираф",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Kaninchen - кролик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Hase - заяц",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Kuh - корова",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Ratte - крыса",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Fuchs - лиса",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Pferd - лошадь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Frosch - лягушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Bär - медведь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Maus - мышь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Affe - обезьяна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Schwein - свинья",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Elefant - слон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Ente - утка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Land - страна; сельская местность; земля",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Russland  - Россия ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Deutschland - Германия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Österreich - Австрия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Schweiz - Швейцария",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Stadt - город",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Haus - дом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Gebäude - здание",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Platz - площадь; место",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Eingang - вход",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Ausgang - выход",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Zentrum - центр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Hof - двор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Dach - крыша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Zaun - забор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Dorf - деревня, поселок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Schule - школа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Universität - университет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Theater - театр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Kirche - церковь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Restaurant - ресторан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Cafe - кафе",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Hotel - гостиница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Bank - банк",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Kino - кинотеатр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Krankenhaus - больница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Polizei - полиция",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Postamt - почта",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Bahnhof - станция, вокзал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Flughafen - аэропорт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Geschäft - магазин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Apotheke - аптека",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Markt - рынок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Büro - офис",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Firma - фирма",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Betrieb - предприятие",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Straße - улица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Weg - дорога",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Kreuzung - перекрёсток",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Haltestelle - остановка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Gehsteig - тротуар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Pfad - тропа, тропинка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Garten - сад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Park - парк",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Brücke - мост",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Fluss - река",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Wald - лес",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Feld - поле",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Berg - гора",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der See - озеро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Meer - море",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Ozean - океан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Küste - морской берег, побережье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Strand - пляж",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Sand - песок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Insel - остров",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Grenze - граница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Zoll - таможня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Müll - мусор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Abfälle - отходы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Stein - камень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Pflanze - растение",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Baum - дерево",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Gras - трава",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Blume - цветок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Blatt - лист",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Wohnung - квартира",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Zimmer - комната",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Wohnzimmer - зал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Schlafzimmer - спальня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Badezimmer - ванная",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Dusche - душ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Toilette - туалет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Küche - кухня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Flur - коридор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Balkon - балкон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Fußboden  - пол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Decke - потолок; одеяло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Wand - стена",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Treppe - лестница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Tür - дверь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Fenster - окно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Fensterbrett - подоконник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Gardine - занавес(ка), штора",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schalter - выключатель; окошко (кассы)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Steckdose - розетка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Wasserhahn - (водопроводный) кран",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Rohr - труба",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schornstein  - дымовая труба",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " die Möbel - мебель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Tisch - стол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Stuhl - стул",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Sessel - кресло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Couch [кáуч] - диван",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Bett - кровать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schrank - шкаф",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Regal - полка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Spiegel - зеркало",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Teppich - ковер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kühlschrank - холодильник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Mikrowelle - микроволновка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Ofen - печь, духовка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Herd - кухонная плита",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Lebensmittel - еда, продукты",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Brot - хлеб",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Butter - сливочное масло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Öl  - растительное масло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Käse - сыр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Wurst - колбаса",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Würstchen - сосиска",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Schinken - ветчина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Fleisch - мясо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Rindfleisch - говядина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Schweinefleisch - свинина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Lammfleisch - баранина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Hähnchen - курица (мясо)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Kotelett - котлета, отбивная",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Fisch - рыба",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Ei - яйцо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Salat - салат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Pilze - грибы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Mais - кукуруза",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Brei - каша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Haferflocken - овсянка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Suppe - суп ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das belegte Brötchen - бутерброд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Reis - рис",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Nudeln - лапша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Mehl - мука",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Gewürz - специя, пряность",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Pfeffer - перец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Salz - соль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Zwiebel - лук (репчатый)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Knoblauch - чеснок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Soße - соус",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Gemüse - овощи",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Kartoffeln - картофель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Mohrrübe - морковь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Zuckerrübe - свекла",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Tomate - помидор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Gurke - огурец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kohl - капуста",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Zucchini [цукини] - кабачок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Aubergine [обэр'жинэ] - баклажан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Bohnen - бобы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Erbsen - горох",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Nuss - орех",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Obst  - фрукты",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Apfel - яблоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Birne - груша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Banane - банан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Beere - ягода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Erdbeere - клубника, земляника",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Himbeere - малина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Kirsche - вишня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Pflaume - слива",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Trauben  - виноград",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Aprikose - абрикос",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Pfirsich - персик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Melone - дыня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Wassermelone - арбуз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kürbis - тыква",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Orange [орáнжэ] - апельсин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Mandarine - мандарин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Zitrone - лимон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Ananas - ананас",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Zucker - сахар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Honig - мёд ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Marmelade - варенье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kuchen - торт, пирог",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Brötchen - булочка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Gebäck - печенье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Süßigkeiten - конфеты, сладости",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Eis - мороженое; лёд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Schokolade - шоколад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Wasser - вода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Sodawasser - газировка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Saft - сок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Wein - вино",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Tee - чай",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kaffee - кофе",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Milch - молоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Sahne - сливки",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Joghurt - йогурт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Quark - творог",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Geschirr - посуда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Tasse - чашка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Glas - стакан; стекло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Becher - кружка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Teller - тарелка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Löffel - ложка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Gabel - вилка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Messer - нож",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Untertasse - блюдце",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Flasche - бутылка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Serviette - салфетка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Topf - кастрюля; горшок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Pfanne - сковородка ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Kessel - чайник; котел",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Mahlzeit - принятие пищи, еда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Frühstück - завтрак",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Mittagessen - обед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Abendessen - ужин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Transport  - транспорт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Flugzeug - самолет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Auto - автомобиль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Straßenbahn - трамвай",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Bus - автобус",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Zug - поезд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Schiff - корабль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Fahrrad - велосипед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Zeit - время",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Minute - минута",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Stunde - час",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Woche - неделя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Jahr - год",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Jahrhundert - век, столетие ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "das Mal - раз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vorgestern - позавчера",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gestern - вчера",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heute - сегодня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "morgen - завтра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "übermorgen - послезавтра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Tag - день",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Morgen - утро ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Nachmittag - время до полудня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Nachmittag - время после полудня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "der Abend - вечер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "die Nacht - ночь",
        "motherLang": "",
        "oneWord": true
    }
]