export default [
    {
        "foreignLang": "la personne - человек",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'homme - мужчина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la femme - женщина; жена",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'enfant - ребенок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le garçon - мальчик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la fille - девочка; дочь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'ami - друг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'invité - гость",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la famille - семья",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le parents - родители",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le père - отец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la mère - мать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le fils - сын",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le mari - муж",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le grand-père - дед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la grand-mère - бабушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le beau-père - тесть, свёкор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la belle-mère - тёща, свекровь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'oncle - дядя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la tante - тетя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le frère - брат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la soeur - сестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le cousin- двоюродный брат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la cousine - двоюродная сестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le travail - работа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'enseignant - учитель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le conducteur - водитель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le travailleur - рабочий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'ingénieur - инженер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le docteur - врач",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'infirmière - медсестра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le vendeur - продавец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le comptable - бухгалтер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le peintre - художник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'étudiant - студент",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le pays - страна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Russie - Россия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "France - Франция",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'animal - животное",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le chat - кошка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le chien - собака",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'oiseau - птица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la ville - город",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'école - школа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le théâtre - театр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la rue - улица; дорога",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la place - площадь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la maison - дом",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'église - церковь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la rivière - река",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le café - кафе",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'hôtel - гостиница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le jardin - сад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le parc - парк",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la banque - банк",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'arrêt - остановка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le cinéma - cinéma",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le pont - мост",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'intersection - перекрёсток",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la forêt - лес",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'hôpital - больница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le marché - рынок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la police - полиция",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la poste - почта",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la gare - станция, вокзал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le centre - центр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le magasin - магазин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la montagne - гора",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le appartement - квартира",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la cuisine - кухня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le balcon - балкон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la salle de bain - ванная",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la douche - душ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "les cabinets - туалет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le sol - пол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le plafond - потолок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le étage - этаж",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le hall - коридор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chambre à coucher - спальня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le salon - зал",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la porte - дверь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la fenêtre - окно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la clé - ключ",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le lit - кровать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la couverture - одеяло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le oreiller - подушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la table - стол",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chaise - стул",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le fauteuil - кресло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le frigo - холодильник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le sofa - диван",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le miroir - зеркало",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la nourriture - еда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le pain - хлеб",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le beurre - масло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le fromage - сыр",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le saucisson - колбаса",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la saucisse - сосиска",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'huile - растительное масло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le poivre - горький перец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le sel - соль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la baie - ягода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le miel - мёд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la confiture - варенье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le champignon - гриб",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'oignon - лук (репчатый)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la banane - banane",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la carotte - морковь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la poire - груша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la betterave - свекла",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "les fruits - фрукты",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le melon - дыня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la pastèque - арбуз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le gâteau - торт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le chocolat - шоколад",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la viande - мясо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la pomme de terre - картофель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la salade - салат",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la tomate - помидор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la concombre - огурец",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le chou - капуста",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la bouillie - каша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le soupe - суп",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le sandwich - бутерброд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le soda - газировка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'eau - вода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le café - кофе",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le thé - чай",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le lait - молоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le jus - сок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la pomme - яблоко",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le raisins - виноград",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'orange - апельсин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'ananas - ананас",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'abricot - абрикос",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le sucre - сахар",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le riz - рис",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "las nouilles - лапша",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le bœuf - говядина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le porc - свинина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le poulet - курица",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la côtelette - котлета",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le citron - лимон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "les pois - горох",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la brioche - булочка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le poisson - рыба",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la pâtisserie - выпечка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le bonbon - конфета",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la glace - мороженое",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le écrou - орех",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le oeuf - яйцо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la pêche - персик",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la tasse - чашка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la verre - стакан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la assiette - тарелка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la cuillère - ложка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la fourchette - вилка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le couteau - нож",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la soucoupe - блюдце",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la bouteille - бутылка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la serviette - салфетка, полотенце",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le petit déjeuner - завтрак",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le déjeuner - обед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le dîner - ужин",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'avion - самолет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la voiture - автомобиль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le tram - трамвай",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le bus - автобус",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le train - поезд",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le vélo - велосипед",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le temps - время",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'année - год",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la semaine - неделя",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'heure - час",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la minute - минута",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la fois - раз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "hier - вчера",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "aujourd'hui - сегодня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "demain - завтра",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le jour - день",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le matin - утро",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la soirée - вечер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la nuit - ночь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Lundi - понедельник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Mardi - вторник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Mercredi - среда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Jeudi - четверг",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Vendredi - пятница",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Samedi - суббота",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Dimanche - воскресенье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le mois - месяц",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Janvier - январь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Février - февраль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Mars - март",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Avril - апрель",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Mai - май",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Juin - июнь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Juillet - июль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Août - август",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Septembre - сентябрь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Octobre - октябрь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Novembre - ноябрь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Décembre - декабрь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le printemps - весна",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'été - лето",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'automne - осень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'hiver - зима",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le nom - имя, фамилия",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'adresse - адрес",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le numéro - номер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'anniversaire - день рождения",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "marié(e) - женатый (замужняя)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chose - вещь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le stylo - ручка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le livre - книга",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "les échecs - шахматы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le téléphone - телефон",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'horloge - часы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le peigne - расчёска",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le téléviseur - телевизор",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le fer - утюг; железо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le savon - мыло",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la radio - radio",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le sac - сумка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la carte - карта; открытка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la valise - чемодан",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le cadeau - подарок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la caméra - камера",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'ordinateur - компьютер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le film - фильм",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la fleur - цветок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le vase - ваза",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'image - картина",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le mouchoir - носовой платок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la balle - мяч",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le ballon - воздушный шар(ик)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le jouet - игрушка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'addition - счёт (в ресторане), (итоговая) сумма",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'enveloppe - конверт",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le papier - бумага",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le journal - газета",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la lettre - письмо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le billet - билет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "les vêtements - одежда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chaussures - обувь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le manteau - пальто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la robe - платье",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chemise - рубашка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la jupe - юбка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le gant - перчатка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le chapeau - шляпа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la veste - пиджак, куртка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'écharpe - шарф",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "las chaussettes - носки",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le pull - свитер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": " le T-shirt - футболка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la cravate - галстук",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le pantalon - брюки",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "que - что",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quel - какой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "qui - кто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "où - где, куда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "d'ou - откуда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "comment - как",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pourquoi - почему",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quand - когда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "je - я",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tu - ты",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "il - он",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "elle - она",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nous - мы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vous - вы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ils, elles - они",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mon - мой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ton - твой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "son - его, её",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "notre - наш",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "votre - ваш",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "leur - их",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "de - из, от; передает род. падеж",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "à - к",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sur - на",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sous - под",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "derrière - позади",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "avec - с",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sans - без",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "avant - перед; до",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "entre - между",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "en - в",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "près de - возле",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "après - после",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pour - для",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "un - один",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "deux - два",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trois - три",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quatre - четыре",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cinq - пять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "six - шесть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sept - семь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "huit - восемь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "neuf - девять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dix - десять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "onze - одиннадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "douze - двенадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "treize - тринадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quatorze - четырнадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quinze - пятнадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "seize - шестнадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dix-sept - семнадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dix-huit - восемнадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dix-neuf - девятнадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vingt - двадцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trente - тридцать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quarante - сорок",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cinquante - пятьдесят",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "soixante - шестьдесят",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quatre-vingts - восемьдесят",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "quatre-vingt-dix - девяносто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cent - сто",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mille - тысяча",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vieux - старый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jeune - молодой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nouveau - новый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "grand - большой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "petit - маленький",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "plein - полный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bon - хороший",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "mauvais - плохой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tôt - ранний",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tard - поздний",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dernier - последний, прошлый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "suivant - следующий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gratuit - бесплатный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "libre - свободный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chaud - жаркий; горячий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chaud - тёплый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "froid - холодный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "haut - высокий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bas - низкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "long - длинный, долгий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "facile - лёгкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lourd - тяжёлый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sombre - тёмный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cher - дорогой",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pas cher - дешёвый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gauche - левый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "droit - справа",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "correct - правильный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rapide - быстрый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lentement - медленный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "doux - мягкий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dur - твёрдый, крепкий, трудный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "belle - красивый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "attentivement - внимательный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "triste - печальный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "heureux - радостный, счастливый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fini - готовый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "en colère - сердитый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "principal - основной, главный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la couleur - цвет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "noir - чёрный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "bleu - голубой; синий",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "brun - коричневый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vert - зелёный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gris - серый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "rouge - красный",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "blanc - белы",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jaune - жёлтый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "oui - да",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "non - нет",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ne pas - не",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cet - этот, тот",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ici - здесь, сюда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "là - там, туда",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "maintenant - сейчас, теперь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "déjà - уже",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "encore - еще",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "beaucoup (de) - много",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "peu (de) - мало",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "très - очень",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chaque - каждый",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tout - все",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tous - всё",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tel - вской",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "et - и",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ou - или",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "parce que - потому что, так как",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "être - быть, являться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "avoir - иметь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pouvoir - мочь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "devoir - быть должным",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "besoin - быть должным (внутренние причины)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "avoir besoin de - нуждаться, нужно",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "devenir - становиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vivre - жить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "venir - приходить, приезжать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "aller - идти, ехать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "conduire - вести (машину), отвозить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "voir - видеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "être assis - сидеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dire - сказать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "parler - разговаривать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "se tenir (debout) - стоять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "travailler - работать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "casser - ломать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "faire - делать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "envoyer - посылать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "acheter - покупать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "nager - плавать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dormir - спать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "réveiller - будить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "laver - мыть, cтирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "essayer - пытаться, пробовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "trouver - находить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "apporter - приносить, привозить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "célébrer - праздновать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "sourire - улыбаться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "pleurer - плакать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "coûter - стоить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "apprendre - учиться",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "enseigner - обучать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "écrire - писать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "changer - менять",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "tomber - падать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "écouter - слушать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "montrer - показывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "gagner - побеждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "penser - думать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "fermer - закрывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "ouvrir - открывать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "savoir - знать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "connaître - быть знакомым",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "chanter - петь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "danser - танцевать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "demander - спрашивать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "recueillir - собирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "aimer - любить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "dessiner - рисовать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "choisir- выбирать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "vouloir - хотеть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "manger - кушать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "boire - пить",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "donner - давать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cuire - печь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "cuisinier - готовить (еду)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "couper - резать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "prendre - брать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "attendre - ждать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "lire - читать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "jouer - играть",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "répondre - отвечать",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Bonjour - Здравствуй(те). Добрый день.",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Merci - Спасибо",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Pardon - Извини(те)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "S'il vous plaît - Пожалуйста (прошу)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "De rien - Пожалуйста (Не за что)",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Quel dommage - Как жаль",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "Au revoir - до свидания",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chanson - песня",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la musique - музыка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la fête - вечеринка, праздник",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la taille - размер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "l'argent - деньги",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la chance - удача, шанс",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la blague - шутка",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la surprise - сюрприз",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le problème - проблема",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la faim  - голод",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la météo - погода",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la pluie - дождь",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le vent - ветер",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "la neige - снег",
        "motherLang": "",
        "oneWord": true
    },
    {
        "foreignLang": "le ciel - небо",
        "motherLang": "",
        "oneWord": true
    }
]