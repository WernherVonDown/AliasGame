export enum Langs {
    RUS = 'russian',
    FR = 'french',
    EN = 'english',
    DE = 'german',
}