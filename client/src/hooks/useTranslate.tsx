import React, { useCallback } from "react";
import { injectIntl } from "react-intl";

interface IProps {
    intl: any;
    // id: string;
}
//@ts-ignore
export const useTranslate = injectIntl(({intl}:IProps) => {
    const translate = useCallback((id: string) => {
        intl.formatMessage({ id })
    }, [intl])

    return {
        translate
    }
})