export const SOCKET_URL = process.env.REACT_APP_SOCKET_URL || "http://localhost:4000";
export const BACKEND_URL = process.env.REACT_APP_BACKEND_URL || "http://localhost:4000";
export const REACT_APP_GOOGLE_AUTH_CLIENT_ID = process.env.REACT_APP_GOOGLE_AUTH_CLIENT_ID || ''
