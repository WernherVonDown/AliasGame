import LOCALES from "../../const/i18n/LOCALES";
import LANGUAGE_KEYS from "../../i18n/messages/LANGUAGE_KEYS";

export default (code: string) => {
    switch(code) {
        case LOCALES.ENGLISH:
            return LANGUAGE_KEYS.english;
        case LOCALES.RUSSIAN:
            return LANGUAGE_KEYS.russian;
        default:
            return LANGUAGE_KEYS.english;
    }
} 