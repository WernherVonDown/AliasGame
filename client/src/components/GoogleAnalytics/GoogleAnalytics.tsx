import React, { useEffect, useRef, useState } from 'react';
import { withRouter } from 'react-router-dom';

export default withRouter((props) => {
    const [prev, setPrev] = useState('/');
    //@ts-ignore
    const gtag = window?.gtag;
    useEffect(() => {
        console.log('AAA', gtag)
        if (props.location.pathname !== prev && typeof(gtag) === 'function') {
            setPrev(props.location.pathname)
            gtag('config', 'G-E1ZDQ8ZBZK', {
                'page_title': document.title,
                'page_location': window.location.href,
                'page_path': window.location.pathname
            });
        }

        
    }, [window.location, props.location])

    return null;
})