import React, { ReactNode, useState } from "react";
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';
import styles from './RulesModal.module.scss';
import translate from "../../i18n/translate";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import { SchoolIcon } from "../../icons/SchoolIcon";

interface IProps {
  children?: ReactNode;
}

export const RulesModal: React.FC<IProps> = ({ children }) => {
  const [open, setOpen] = useState(false);

  const onOpenModal = () => setOpen(true);
  const onCloseModal = () => setOpen(false);

  return (
    <div>
      <div className={styles.openButton} onClick={onOpenModal}>{children ? children : <SchoolIcon />}</div>
      <Modal open={open} onClose={onCloseModal} center>
        <h2>{translate(MESSAGES_KEYS.rules_text_title)}</h2>
        <h4>{translate(MESSAGES_KEYS.rules_text_title2)}</h4>
        <div className={styles.text}>
          {translate(MESSAGES_KEYS.rules_text)}
        </div>
      </Modal>
    </div>
  );
}