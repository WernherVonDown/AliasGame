import React, { useContext, useEffect, useState } from "react";
import { Button, TextInput } from "react-materialize";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../context/auth.context";
import useInput from "../../hooks/useInput";
import MainHeader from "../MainHeader/MainHeader";
import styles from './AddCustomWords.module.scss';
import { UserContext } from "../../context/user.context";
import { PrimaryInput } from "../inputs/PrimatyInput/PrimaryInput";
import { PrimaryButton } from '../buttons/PrimaryButton';
import { MainLayout } from "../Layouts/MainLayout/MainLayout";

const AddCustomWords = () => {
    const { actions: authActions, state: authState } = useContext(AuthContext);
    const { actions: userActions } = useContext(UserContext)
    const history = useHistory();
    const wordsName = useInput('Новый список');
    const word = useInput('');
    const [words, setWords] = useState<string[]>([])
    useEffect(() => {
        console.log('AddCustomWords', authState.loggedIn)
        if (!authState.loggedIn) {
            history.push('/')
        }
    }, [authState.loggedIn])
    const send = () => {
        const r = {
            "name": "user sdfssss dfsdsdfsd",
            "words": [{
                "foreignLang": "Helloef = sdfs",
                "motherLang": "",
                "oneWord": true
            },
            {
                "foreignLang": "1231231 sdfsdfs",
                "motherLang": "sdfsd sdf",
                "oneWord": false
            }
            ]
        }
        const userWords = words.map(w => {
            return {
                foreignLang: w,
                motherLang: '',
                oneWord: true,
            }
        })

        const wordsData = {
            name: wordsName.value,
            words: userWords
        }

        userActions.addCustomWords(wordsData);
    }

    const onAddWord = () => {
        if (!word.value.length) {
            return alert('Заполните поле')
        }

        setWords([...words, word.value])
        word.clear()
    }
    return <MainLayout>
        <div className={styles.content}>
        <div className={styles.formWrapper}>
            {/* <div className={styles.titleText}>Добавте свои слова</div> */}

            <PrimaryInput type="text" {...wordsName} placeholder="Название списка" />
            <div className={styles.addWordWrapper}>
                <PrimaryInput className={styles.input} placeholder={"Добавьте слово"} type="text" {...word} />
                <PrimaryButton onClick={onAddWord}>
                    +
                </PrimaryButton>
            </div>
            <div className={styles.wordsList}>
                {words.map((w, i) => {
                    return <div className={styles.wordsListItem} key={`word_${w}_${i}`}>
                        {w}
                    </div>
                })}
            </div>
            <div className={styles.btnWrapper}>
                
                {/* <Button
                    onClick={() => history.push('/')}
                > На главную
                </Button> */}
            </div>
            
        </div>
        <PrimaryButton onClick={send}>Сохранить список</PrimaryButton>
        </div>
        </MainLayout>
}

export default AddCustomWords;