import React, { useState, useEffect, useContext, useMemo, useRef, useCallback } from "react";
import { Button, Caption, Carousel, Checkbox, Select, Slide, Slider } from "react-materialize";
import SwapHorizIcon from '../../icons/SwapHorizIcon/SwapHorizIcon';
import styles from './CreateGame.module.scss'
import { Langs } from '../../const/languages/LANGS';
import RusFlag from '../../icons/Flags/RusFlag';
import shorid from 'shortid';
import { IRoomCreate, ICheckRoomResult } from '../../const/room/types';
import { useSockets } from '../../context/socket.context';
import { ROOM_EVENTS } from '../../const/room/ROOM_EVENTS';
import { useHistory } from "react-router-dom";
import Header from "../Header/Header";
import MainHeader from "../MainHeader/MainHeader";
import { UserContext } from "../../context/user.context";
import { AuthContext } from "../../context/auth.context";
import GoogleAuthButton from "../common/GoogleAuthButton";
import translate from "../../i18n/translate";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import { FormattedMessage, injectIntl, WithIntlProps } from "react-intl";
import { CreateGameContext } from "../../context/createRoom.context";
import { PrimaryButton } from '../buttons/PrimaryButton';
import { PrimarySelect } from "../PrimarySelect/PrimarySelect";

interface IProps {
    intl: any;
}

const CreateGame:React.FC<WithIntlProps<IProps>> = injectIntl(({intl}) => {
    const { state: userState, actions: userActions } = useContext(UserContext)
    const { state: authState } = useContext(AuthContext)
    const { state: createGameState, actions: { setForeign, startGame } } = useContext(CreateGameContext);
    // const [foreignLang, setForeign] = useState(Langs.EN);
    // const [motherLang, setMotherLang] = useState(Langs.RUS);
    const [useCustomWords, setUseCustomWords] = useState(false);
    console.log('USE')
    const [customWords, setCustomWords] = useState('')
    const cWRef = useRef('')
    const foreignLangRef = useRef('');

    const { socket } = useSockets();
    const history = useHistory();

    useEffect(() => {
        subscibe();
        console.log('MOUNT')
        return () => {
            console.log('UNMOUNT')
        }
    }, [])

    useEffect(() => {
        if (authState.loggedIn) {
            userActions.getCustomWords()
        }
    }, [authState.loggedIn])

    const unsubscribe = () => {

    }

    const subscibe = () => {
        socket.on(ROOM_EVENTS.checkRoom, checkRoom.bind(this))
        socket.on(ROOM_EVENTS.createRoom, roomCreated)
    }

    const roomCreated = (data: any) => {
        console.log("EEEEEEEEE", data)
        if (data.success) {
            history.push(`/room/${data.roomId}`);
        }
    }

    useEffect(() => {
        if (useCustomWords && !customWords.length && userState.customWords.length) {
            console.log('SET CUSOM WORDS', userState.customWords[0]._id)
            setCustomWords(userState.customWords[0]._id)
        }
    }, [userState.customWords, customWords, useCustomWords])

    const checkRoom = (data: ICheckRoomResult) => {
        const { roomId, result } = data;
        const { foreignLang, motherLang } = createGameState;
        console.log('checkRoom', { roomId, result, customWords }, cWRef.current, createGameState, foreignLangRef.current)
        startGame()
        if (!result) {

            const createRoomData: IRoomCreate = {
                roomId,
                langs: {
                    foreignLang: foreignLangRef.current || foreignLang,
                    motherLang
                },
                custom: customWords || cWRef.current
            }
            console.log("CREATE ROO", createRoomData)
            socket.emit(ROOM_EVENTS.createRoom, createRoomData);
        } else {
            createGame();
        }
    }

    const createGame = useCallback(() => {
        const roomId = shorid.generate();
        cWRef.current = customWords;
        console.log('CREATE R', cWRef.current)
        socket.emit(ROOM_EVENTS.checkRoom, roomId);
    }, [])

    const onUseCustomChanged = (e: any) => {
        setUseCustomWords(!useCustomWords)
    }

    const getCustomWords = () => {
        if (!authState.loggedIn) {
            return (
                <div>
                    {translate(MESSAGES_KEYS.enter_to_add_custom_lists)}
                    <div className={styles.authWraper}>
                        <PrimaryButton onClick={() => history.push('/login')}>
                            {translate(MESSAGES_KEYS.login)}
                        </PrimaryButton>
                        {/* <GoogleAuthButton /> */}
                    </div>

                </div>
            )
        } else {
            if (userState.customWords.length) {
                console.log('EEE CREATE', userState.customWords)
                const options = userState.customWords.map(e => ({ value: e._id, label: e.name }))

                // <PrimarySelect
                //     options={options}
                //     values={[options.find(e => e.value === customWords) || options[0]]}
                //     onChange={(e) => setCustomWords(e[0].value)}
                // />

                return <div className={styles.selectWrapper}>
                    <PrimarySelect
                        options={options}
                        values={[options.find(e => e.value === customWords) || options[0]]}
                        onChange={(e) => setCustomWords(e[0].value)}
                    />
                    {/* <Select
                        id="Select-32"
                        multiple={false}
                        onChange={
                            (e) => {
                                console.log('EEE SET', e.target.value);
                                setCustomWords(e.target.value);
                                console.log("AFTER", customWords)
                            }
                        }
                        options={{
                            classes: '',
                            dropdownOptions: {
                                alignment: 'left',
                                autoTrigger: true,
                                closeOnClick: true,
                                constrainWidth: true,
                                coverTrigger: true,
                                hover: false,
                                inDuration: 150,
                                outDuration: 250
                            }
                        }}
                        value={userState.customWords[0]._id}
                    >
                        {userState.customWords.map(w => {
                            console.log('RENDER W', w)
                            return <option key={w._id} value={w._id}>
                                {w.name}
                            </option>
                        })}

                    </Select> */}
                    <Button onClick={() => history.push('/words/add')}>Добавить</Button>
                </div>
            } else {
                return <div>У вас пока нет слов<Button onClick={() => history.push('/words/add')}>Добавить</Button></div>
            }
        }
    }

    const getWords = () => {
        if (!useCustomWords) {
            const options = [
                {
                    value: Langs.EN,
                    // locKey: MESSAGES_KEYS.english,
                    label: `🇺🇸 ${intl.formatMessage({ id: MESSAGES_KEYS.english })}`
                },
                {
                    value: Langs.FR,
                    // locKey: MESSAGES_KEYS.french,
                    label: `🇫🇷 ${intl.formatMessage({ id: MESSAGES_KEYS.french })}`
                },
                {
                    value: Langs.DE,
                    // locKey: MESSAGES_KEYS.german,
                    label: `🇩🇪 ${intl.formatMessage({ id: MESSAGES_KEYS.german })}`
                }
            ]
            const optionsMother = [
                {
                    value: Langs.RUS,
                    // locKey: MESSAGES_KEYS.russian,
                    label: `🇷🇺 ${intl.formatMessage({ id: MESSAGES_KEYS.russian })}`
                }
            ]

            console.log("SELECTED", options.find(e => e.value === createGameState.foreignLang) || options[0])
            return (

                <div className={styles.selectLangPairWrapper}>

                    <div className={styles.selectWrapper}>
                    <PrimarySelect
                        className={styles.select}
                        options={options}
                        dropdownHandleRenderer={(e) => <span style={{width: 0}}/>}
                        values={[options.find(e => e.value === createGameState.foreignLang) || options[0]]}
                        onChange={(e) => { setForeign(e[0].value); foreignLangRef.current = e[0].value; }}
                    />

                    </div>
                    <div><SwapHorizIcon className={styles.swapIcon} /></div>
                    <div className={styles.selectWrapper}>
                    <PrimarySelect
                        className={styles.select}
                        // wrapperClassName={styles.selectWrapper}
                        dropdownHandleRenderer={(e) => <span style={{width: 0}}/>}
                        options={optionsMother}
                        values={[optionsMother.find(e => e.value === createGameState.motherLang) || optionsMother[0]]}
                        onChange={(e) => { }}
                    />

                    </div>
                </div>
            )
        }

        return getCustomWords()

    }

    return (
        <div className={styles.createGame}>
            <div className={styles.createGameTitle}>
                {translate(MESSAGES_KEYS.choose_language_pair)}
            </div>
            {getWords()}
            <div className={styles.customWordsWrapper}>

                <Checkbox
                    id="Checkbox_1"
                    label=''
                    value="custom"
                    onChange={onUseCustomChanged}
                    className={styles.customWordsCheckbox}
                />{translate(MESSAGES_KEYS.use_custom_words)}
            </div>
            <div>
                <PrimaryButton onClick={createGame} className={styles.enterButton}>{translate(MESSAGES_KEYS.enter)}</PrimaryButton>
            </div>
        </div>)
})

export default CreateGame;