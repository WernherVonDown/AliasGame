import React from "react"
import CamIcon from "../../icons/CamIcon/CamIcon";
import MicIcon from "../../icons/MicIcon/MicIcon";
import styles from './VideoChatControlls.module.scss'
import { useContext } from 'react';
import { VideoChatContext } from "../../context/videoChat.context";
import { ExitIcon } from "../../icons/ExitIcon";
import { RulesModal } from "../RulesModal/RulesModal";

const VideoChatControlls = () => {
    const { actions: { changeEnabledDevicesTypes }, state: { enabledDevicesTypes } } = useContext(VideoChatContext);

    return <div className={styles.videoChatControlls}>
        <div>
        <div className={styles.controllButton}>
            <RulesModal />
        </div>
        </div>
        <div>
            <div className={styles.controllButton} onClick={() => changeEnabledDevicesTypes({ ...enabledDevicesTypes, video: !enabledDevicesTypes.video })}>
                <CamIcon active={enabledDevicesTypes.video} />
            </div>
        </div>
        <div>
            <div className={styles.controllButton} onClick={() => changeEnabledDevicesTypes({ ...enabledDevicesTypes, audio: !enabledDevicesTypes.audio })}>
                <MicIcon active={enabledDevicesTypes.audio} />
            </div>
        </div>
        <div>
            <a className={styles.controllButton} href="/">
                <ExitIcon  />
            </a>
        </div>
    </div>
}

export default VideoChatControlls;