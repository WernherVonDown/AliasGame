import React, { useMemo } from "react";
import styles from './PrimarySelect.module.scss'
import Select, {SelectItemRenderer, SelectProps, SelectRenderer} from 'react-dropdown-select';
import classNames from "classnames";
import { injectIntl, WithIntlProps } from "react-intl";

interface IProps<T> {
    options: T[];
    values: T[];
    onChange: (value: T[]) => void;
    itemRenderer?: ({
      item,
      itemIndex,
      props,
      state,
      methods
    }: SelectItemRenderer<T>) => JSX.Element;
    optionRenderer?: ({ item, props, state, methods }: SelectItemRenderer<T>) => JSX.Element;
    className?: string;
    intl: any;
    dropdownHandleRenderer?: ({ props, state, methods }: SelectRenderer<T>) => JSX.Element;
    wrapperClassName?: string;
}


export const PrimarySelect: React.FC<WithIntlProps<IProps<any>>> = injectIntl((
    {options, values,wrapperClassName, itemRenderer, onChange, className, optionRenderer, intl, dropdownHandleRenderer}
    ) => {
        const preparedOptions = useMemo(() => {
            return options.map(e => ({...e, label: e.locKey ? intl.formatMessage({ id: e.locKey }): e.label}))
        }, [options])
    return (
        <Select
            options={preparedOptions}
            values={values}
            itemRenderer={itemRenderer}
            onChange={onChange}
            className={classNames(styles.select,'browser-default',className)}
            optionRenderer={optionRenderer}
            dropdownHandleRenderer={dropdownHandleRenderer}
            dropdownGap={0}
            wrapperClassName={wrapperClassName}
            style={{width: '100%'}}
        />
    )
})