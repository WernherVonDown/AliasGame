import classNames from 'classnames';
import React, { useContext } from 'react';
import GoogleLogin from 'react-google-login';
import { Button } from 'react-materialize';
import { REACT_APP_GOOGLE_AUTH_CLIENT_ID } from '../../config/default';
import { AuthContext } from '../../context/auth.context';
import GoogleIcon from '../../icons/GoogleIcon';
import styles from './GoogleAuthButton.module.scss';
import { PrimaryButton } from '../buttons/PrimaryButton';

const GoogleAuthButton = () => {
    const { actions: { googleAuth } } = useContext(AuthContext);

    const onSuccess = (res: any) => {
        const {profileObj: {givenName, email}, accessToken} = res;
        console.log("ON SUCCESS", res)
        googleAuth(givenName, email, accessToken)
    }
    return (
        <GoogleLogin
            clientId={REACT_APP_GOOGLE_AUTH_CLIENT_ID}
            render={renderProps => (
                <PrimaryButton
                    onClick={renderProps.onClick}
                    className={classNames(styles.googleBtn)}
                >
                    <GoogleIcon className={styles.googleIcon} />
                    Войти с Google
                </PrimaryButton>
            )}
            buttonText="Login"
            onSuccess={onSuccess}
            onFailure={(e) => console.log("ON FAIL", e)}
            cookiePolicy={'single_host_origin'}
        />
    )
}

export default GoogleAuthButton;