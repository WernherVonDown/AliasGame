import classNames from 'classnames';
import React, { useEffect, useContext, MutableRefObject, useRef, useState, Fragment } from 'react'
import { IUser } from '../../const/user/types';
import { VideoChatContext } from '../../context/videoChat.context';
import styles from './Video.module.scss'

interface IProps {
    stream: MediaStream | undefined;
    muted?: boolean;
    className?: string;
    userName?: string;
    teamColor?: string;
    isMe?: boolean;
    tracksEnabled: {video: boolean, audio: boolean}
}

const Video = (props: IProps) => {
    const  {state: {enabledDevicesTypes} } = useContext(VideoChatContext);
    const { stream, muted, className = '', userName, teamColor, isMe, tracksEnabled } = props;
    const videoRef: MutableRefObject<any> = useRef(null)
    const audioRef: MutableRefObject<any> = useRef(null)
    const [videoDesabled, setVideoDesabled] = useState(false);

    useEffect(() => {
        if (!isMe)
        console.log('EFFECT', userName,stream, videoRef.current, stream && stream?.getVideoTracks()[0]?.enabled, stream && stream?.getVideoTracks()[0])
        if (videoRef.current && stream){
            console.log('VIDEO', stream?.active)
            videoRef.current.srcObject = stream;
        } 
        // else if (audioRef.current && !enabledDevicesTypes.video) {
        //     audioRef.current.srcObject = stream;
        // }
    }, [stream, tracksEnabled.video, enabledDevicesTypes.video])

    // useEffect(() => {
    //     console.log('ENABLED EFFECT', enabledDevicesTypes)
    //     // if (videoDesabled !== !enabledDevicesTypes.video) {
    //     //     setVideoDesabled(true)
    //     // }
    // }, [stream])
    if (!stream || (!tracksEnabled.video || (isMe && !enabledDevicesTypes.video))) {
        return <div style={{background: teamColor}} className={classNames(styles.audioWrapper, className)}>
                {userName && <div className={styles.nameLable}>{userName}</div>}
            <audio  ref={audioRef} autoPlay muted={muted}/>
        </div>
    }
    return <Fragment>{userName && <div className="user--name">{userName}</div>}<video className={classNames(className)} ref={videoRef} muted={muted} autoPlay/></Fragment>
}

export default Video;