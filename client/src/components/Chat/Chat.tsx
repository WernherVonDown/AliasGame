import React from "react"
import { Button, TextInput, Icon } from "react-materialize";
import MessagesList from "./MessagesList";
import styles from './Chat.module.scss';
import useInput from '../../hooks/useInput';
import { useContext } from 'react';
import { ChatContext } from "../../context/chat.context";
import useForceUpdate from '../../hooks/useForceUpdate';
import { useRef } from "react";
import { useEffect } from "react";
import { FormSelect } from "materialize-css";
import translate from "../../i18n/translate";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import { PrimaryInput } from '../inputs/PrimatyInput/PrimaryInput';
import { SendIcon } from "../../icons/SendIcon";

const Chat = () => {
    const message = useInput('');
    const forceUpdate = useForceUpdate()

    const { actions: chatActions } = useContext(ChatContext);

    const sendMessage = () => {
        chatActions.sendMessage(message.value);
        console.log('EEEE', message)
        message.clear()
    }
    // translate(MESSAGES_KEYS.enter_your_message)
    const handleKeyDown = (e: any) => {
        console.log("HANDLE")
        if (e.key == 'Enter') {
            console.log('AAAAAAAAAa', message.value)
            sendMessage()
        }
    }

    return <div className={styles.chatWrapper}>
        <MessagesList />
        <div className={styles.chatInputWrapper}>
            <PrimaryInput className={styles.input} type="text" onKeyDown={handleKeyDown} placeholder={'сообщение'} {...message} />
            <div className={styles.send} onClick={sendMessage}>
                <SendIcon />
            </div>
        </div>
    </div>
}

export default Chat;