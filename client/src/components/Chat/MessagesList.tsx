import React, { useRef, useEffect } from "react"
import { IMessage } from '../../const/textChat/types';
import Message from './Message';
import styles from './Chat.module.scss';
import { useContext } from 'react';
import { ChatContext } from "../../context/chat.context";
import { NoMsgEmoji } from "../../icons/NoMgsEmoji";
import classNames from "classnames";
import translate from "../../i18n/translate";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";

const MessagesList = () => {
  const { state: { messages } } = useContext(ChatContext)

  const messagesEndRef = useRef<null | HTMLDivElement>(null)

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
  }

  useEffect(() => {
    scrollToBottom()
  }, [messages]);

  return <div className={classNames(styles.chatMessageList, {[styles.noMsg]: !messages.length})}>
    {!messages.length && <div className={styles.noMsgEmoji}>
      <NoMsgEmoji />
      <div>
        {translate(MESSAGES_KEYS.no_message_yet)}
      </div>
    </div>}
    {messages.map(({ userName, text, isMe }: IMessage | any, id: number) => (
      <Message key={id + '_message'} userName={userName} text={text} isMe={isMe} />
    ))}
    <div ref={messagesEndRef} />
  </div>
}

export default MessagesList;