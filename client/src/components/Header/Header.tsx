import React, {useEffect, useRef} from "react";
import styles from './Header.module.scss';
import { useResize } from '../../hooks/useResize';
interface IProps {
    children: any
}

const Header = ({children}: IProps) => {
    const headerRef = useRef(null);
    const { rect: headerRect } = useResize(headerRef);
    useEffect(() => {
        if (headerRef.current) {
            document.documentElement.style.setProperty("--header-height", `${headerRect?.height}px`);
        }
    }, [headerRef, headerRect])
    return <div ref={headerRef} className={styles.header}>
        {children}
    </div>
}

export default Header;