import React, { ReactNode } from "react";
import { Button } from "react-materialize";
import styles from './buttons.module.scss';
import classNames from 'classnames';

interface IProps {
    onClick?: (e: any) => void;
    className?: string;
    children: ReactNode | string;
}

export const PrimaryButton: React.FC<IProps> = ({ onClick, className, children }) => {
    return <button
        onClick={onClick}
        className={classNames(styles.primary, styles.btn,className)}
    >
        {children}
    </button>
}