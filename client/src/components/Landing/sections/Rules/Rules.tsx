import React from "react";
import styles from './Rules.module.scss';
import classNames from 'classnames';


export const Rules: React.FC = () => {
    return (
        <div className={styles.container}>
            <div className={classNames(styles.content, styles.rulesContent)}>
                <div className={styles.title}>Об игре</div>
                <div className={styles.text}>
                Всем известная игра Alias как ни одна другая подходит для развития разговорных навыков. Самое сложное в изучении иностранного языка — это начать говорить. Даже имея возможность практиковаться, например, с другими людьми, изучающими язык, возникают трудности в выборе темы, из-за этого такое общение очень сковывает, выглядит неловко и в конце концов ни к чему не приводит. Игра Alias из-за специфики подходит для любых уровней владения языком. Можно пытаться объяснить слово даже на пальцах, т.к. в игре присутсвует видеочат. Игровая составляющая и игра в команде позволяет раскрепоститься и в веселой обставке начать применять теоретические знания, приобретенные при изучении языка.
                </div>
            </div>

        </div>
    )
}