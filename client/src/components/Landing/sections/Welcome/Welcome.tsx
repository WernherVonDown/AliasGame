import React from "react";
import styles from './Welcome.module.scss';
import classNames from 'classnames';
import { PrimaryButton } from "../../../buttons/PrimaryButton";
import Lottie from 'react-lottie';
import LandingAnimation from '../../../../lottie/landing_animation.json';
import { Rules } from "../Rules/Rules";
import { SecondaryButton } from "../../../buttons/SecondaryButton";
import translate from "../../../../i18n/translate";
import MESSAGES_KEYS from "../../../../i18n/messages/MESSAGES_KEYS";
import { RulesModal } from "../../../RulesModal/RulesModal";

export const Welcome: React.FC = () => {
    return (
        <div className={styles.container}>
            {/* <> */}
            <div className={classNames(styles.content)}>
                <div className={styles.title}>
                    <div>
                        <div>Общайся.</div>
                        <div>Играй в ALIAS.</div>
                        <div>Практикуй  <span style={{ color: 'var(--primary)' }}>язык.</span></div>
                    </div>

                    <div className={styles.subtitle}>Начни разговаривать на английском, французском или немецком вместе с друзьями, просто играя в Alias</div>
                    <div className={styles.buttons}><PrimaryButton className={styles.playButton} onClick={(e: any) => {
                        document.getElementById('startGameSection')?.scrollIntoView({ behavior: 'smooth', block: 'start' })
                    }}>
                        Играть
                    </PrimaryButton>
                    <RulesModal>
                        <SecondaryButton className={styles.playButton}>{translate(MESSAGES_KEYS.rules)}</SecondaryButton>
                    </RulesModal>
                    </div>
                </div>



            </div>
            <div className={styles.content}>
                {/* <div className={styles.rightContent}></div> */}
                <Lottie options={{
                    loop: true,
                    autoplay: true,
                    animationData: LandingAnimation,
                    rendererSettings: {
                        preserveAspectRatio: 'xMidYMid slice'
                    }
                }}
                // height={400}
                // width={400}
                speed={0.7}
                isStopped={false}
                isPaused={false}/>
            </div>
            {/* </> */}

        </div>
    )
}