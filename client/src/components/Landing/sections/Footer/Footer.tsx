import React, { useMemo } from "react";
import styles from './Footer.module.scss'

export const Footer: React.FC = () => {
    const year = useMemo(() => {
        return new Date().getFullYear();
    }, [])
    return <div className={styles.footer}>
        <div className={styles.item}>
            Написать разработчику
        </div>
        <div className={styles.item}>
            Aliasgame, 2021-{year}
        </div>
    </div>
}