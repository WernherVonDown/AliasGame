import React from "react";
import Donate from "../Donate/Donate";
import { MainLayout } from "../Layouts/MainLayout/MainLayout";
import MainHeader from "../MainHeader/MainHeader";
import styles from './Landing.module.scss'
import { Footer } from "./sections/Footer/Footer";
import { Rules } from "./sections/Rules/Rules";
import { StartGame } from "./sections/StartGame/StartGame";
import { Welcome } from './sections/Welcome/Welcome';
import { motion } from "framer-motion";

export const Landing: React.FC = () => {
    return (
        <>
        <motion.div
        exit={{opacity: 0}}
        animate={{opacity: 1}}
        initial={{opacity: 0}} 
        className={styles.landing}>
        {/* <> */}
            <section className={styles.fullscreen}>
                <MainHeader />
                <Welcome />
            </section>
            <section className={styles.fullscreen}>
                <StartGame />
            </section> 
            <div className={styles.rules}>
                <Rules />
            </div>
            <div className={styles.donate}>
                <Donate />
            </div>
            
             
        {/* </> */}

        {/* //     <section className={styles.fullscreen}>
        //         Section 1
        //     </section>  */}
    </motion.div>
    <Footer />
    </>
    )
}