import React, { ReactNode } from "react";
import { Footer } from "../../Landing/sections/Footer/Footer";
import MainHeader from "../../MainHeader/MainHeader";
import styles from './MainLayout.module.scss';
import { motion } from "framer-motion";
import { pageTransition } from "../../../const/animation/pageTransition";
interface IProps {
    children: ReactNode
}

export const MainLayout: React.FC<IProps> = ({ children }) => {
    return (
        <motion.div
        exit="out"
        animate="in"
        initial="out"
        variants={pageTransition}
        className={styles.container}>
            <div className={styles.main}>
                <MainHeader />
                <div className={styles.content}>
                    {children}
                </div>
            </div>
            <Footer />
        </motion.div>
    )
}