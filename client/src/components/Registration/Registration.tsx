import React, { useContext, useEffect } from "react";
import { Button, TextInput } from "react-materialize";
import { useHistory } from "react-router-dom";
import MainHeader from "../MainHeader/MainHeader";
import styles from './Registration.module.scss';
import useInput from '../../hooks/useInput';
import { AuthContext } from "../../context/auth.context";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import translate from "../../i18n/translate";
import { MainLayout } from "../Layouts/MainLayout/MainLayout";
import { PrimaryButton } from "../buttons/PrimaryButton";
import classNames from "classnames";
import GoogleAuthButton from "../common/GoogleAuthButton";
import { PrimaryInput } from "../inputs/PrimatyInput/PrimaryInput";

const Registration = () => {
    const { actions: authActions, state: authState } = useContext(AuthContext);
    const history = useHistory();
    const email = useInput('');
    const pass1 = useInput('');
    const pass2 = useInput('');
    const login = useInput('');

    useEffect(() => {
        if (authState.loggedIn) {
            history.push('/')
        }
    }, [authState.loggedIn])

    const send = async () => {
        if (!email.value.length || !login.value.length || !pass1.value.length) {
            return alert('Заполните все поля')
        }

        if (pass1.value !== pass2.value) {
            return alert('Пароли не совпадают')
        }

        const success = await authActions.register(login.value, email.value, pass2.value);
        if (success) {
            email.clear()
            login.clear()
            pass1.clear()
            pass2.clear()
            history.push('/login')
        }
    }

    return (
        <MainLayout>
            <div>
                <div className={styles.formWrapper}>
                    <div className={styles.titleText}>{translate(MESSAGES_KEYS.register)}</div>
                    <PrimaryInput type={'text'} {...email} translatedPlaceholder={MESSAGES_KEYS.email} />
                    <PrimaryInput type={'text'} {...login} translatedPlaceholder={MESSAGES_KEYS.name} />
                    <PrimaryInput type={'password'} {...pass1} translatedPlaceholder={MESSAGES_KEYS.password} />
                    <PrimaryInput type={'password'} {...pass2} translatedPlaceholder={MESSAGES_KEYS.password} />
                    <div className={styles.btnWrapper}>
                        <PrimaryButton className={styles.btn} onClick={send}>{translate(MESSAGES_KEYS.register)}</PrimaryButton>
                        <div className={styles.orText}>Или</div>
                        <GoogleAuthButton />
                    </div>
                </div>
                <div className={styles.fromFooter}>
                    <div className={styles.fromFooterItem}>
                        <div>Есть аккаунт?</div> <a href="/login">Войти</a>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default Registration;