import React, { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { Button, Select } from "react-materialize";
import { useHistory } from "react-router-dom";
import LOCALES from "../../const/i18n/LOCALES";
import { AuthContext } from "../../context/auth.context";
import { LanguageContext } from "../../context/language.context";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import translate from "../../i18n/translate";
import getLangTranslateKeyByCode from "../../utils/helpers/getLangTranslateKeyByCode";
import GoogleAuthButton from "../common/GoogleAuthButton";
import Header from "../Header/Header";
import styles from './MainHeader.module.scss';
import { LogoIcon } from '../../icons/LogoIcon';
import { SelectLanguage } from "../SelectLanguge/SelectLanguage";
import { PrimaryButton } from "../buttons/PrimaryButton";
import { SecondaryButton } from '../buttons/SecondaryButton';
import { RulesModal } from "../RulesModal/RulesModal";

const MainHeader = () => {
    const history = useHistory();
    const { state: authState, actions: { logout } } = useContext(AuthContext);
    return (
        <Header>
            <div className={styles.headerLeft}>
                <div className={styles.headerLogo} onClick={() => history.push('/')}><LogoIcon /></div>
                <RulesModal />
            </div>
            {
                authState.loggedIn && <div className={styles.rightBtnsWrapper}>
                    <SelectLanguage />
                    {authState.username}
                    <Button
                        onClick={logout}
                        flat
                        className={styles.btn}
                    >
                        {translate(MESSAGES_KEYS.logout)}
                    </Button>
                </div>
            }
            {!authState.loggedIn && <div className={styles.rightBtnsWrapper}>
                <SelectLanguage />
                <SecondaryButton
                    onClick={() => history.push('/registration')}
                >
                    {translate(MESSAGES_KEYS.register)}
                </SecondaryButton>
                <PrimaryButton
                    onClick={() => history.push('/login')}
                >
                    {translate(MESSAGES_KEYS.login)}
                </PrimaryButton>
                {/* <GoogleAuthButton /> */}
            </div>}
        </Header>
    )
}

export default MainHeader;