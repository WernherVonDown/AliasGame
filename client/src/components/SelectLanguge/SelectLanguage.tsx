import React, { useContext } from "react";
import { FormattedMessage } from 'react-intl';
import LOCALES from "../../const/i18n/LOCALES";
import { LanguageContext } from "../../context/language.context";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import { DropdownArrow } from "../../icons/DropdownArrow";
import { PrimarySelect } from "../PrimarySelect/PrimarySelect";
import styles from './SelectLanguge.module.scss';

export const SelectLanguage: React.FC = () => {
    const { actions: { changeLanguage }, state: { currentLanguage } } = useContext(LanguageContext)
    const options = [
        {
            value: LOCALES.ENGLISH,
            // locKey: MESSAGES_KEYS.english,
            label: '🇺🇸 Eng'

        },
        {
            value: LOCALES.RUSSIAN,
            // locKey: MESSAGES_KEYS.russian,
            label: '🇷🇺 Rus'
        }
    ]

    const renderer = (data: any) => {
        console.log("RENDERRER", data)
        return <div className={styles.selectOption} style={{ background: 'var(--background)' }} onClick={() => data.methods.addItem(data.item)}>
            {data.item.label}
        </div>
    }

    return <PrimarySelect
        options={options}
        onChange={(e) => changeLanguage(e[0].value)}
        className={styles.selectLanguage}
        values={[options.find(e => e.value === currentLanguage) || options[0]]}
        optionRenderer={renderer}
        itemRenderer={renderer}
        dropdownHandleRenderer={({ state }) => {
            return <span className={styles.selectDropdownIcon}>
                {state.dropdown ? <DropdownArrow />
                    : <DropdownArrow />
                }</span>
        }}
    />

    return (
        <select value={currentLanguage} className={styles.selectLanguage} onChange={(e) => changeLanguage(e.target.value)}>
            <FormattedMessage id={MESSAGES_KEYS.english}>{
                (msg) =>
                    <option value={LOCALES.ENGLISH}>
                        {msg}
                    </option>
            }
            </FormattedMessage>
            <FormattedMessage id={MESSAGES_KEYS.russian}>{
                (msg) =>
                    <option value={LOCALES.RUSSIAN}>
                        {msg}
                    </option>
            }
            </FormattedMessage>
        </select>
    )
}