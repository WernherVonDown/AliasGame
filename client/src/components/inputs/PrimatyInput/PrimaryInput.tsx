import { formatMessage } from "@formatjs/intl";
import classNames from "classnames";
import React from "react";
import intl, { FormattedMessage, injectIntl, WithIntlProps } from "react-intl";
import styles from './PrimaryInput.module.scss'

interface IProps {
    placeholder?: any;
    type?: string;
    onChange?: (e: React.ChangeEvent<HTMLInputElement> | any) => void;
    value?: string | number;
    id?: string;
    className?: string;
    clear?: () => void;
    translatedPlaceholder?: string;
    intl: any;
    onKeyDown?: (e: any) => void;
}

export const PrimaryInput: React.FC<WithIntlProps<IProps>> = injectIntl((props) => {
    const { className, translatedPlaceholder, clear, intl, onKeyDown, ...inputProps } = props;

    if (translatedPlaceholder) {
        return <input
            tabIndex={0}
            onKeyDown={onKeyDown}
            {...{ ...inputProps, placeholder: intl.formatMessage({ id: translatedPlaceholder }) }}
            className={classNames(styles.input, 'browser-default', className)}
        />
    }
    return (
        <input {...inputProps} className={classNames(styles.input, 'browser-default', className)} />
    )
})