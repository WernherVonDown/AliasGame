import React, { useContext, useEffect } from "react";
import { FormattedMessage } from "react-intl";
import { Button, TextInput } from "react-materialize";
import { useHistory } from "react-router-dom";
import { AuthContext } from "../../context/auth.context";
import useInput from "../../hooks/useInput";
import messages from "../../i18n/messages";
import MESSAGES_KEYS from "../../i18n/messages/MESSAGES_KEYS";
import MainHeader from "../MainHeader/MainHeader";
import styles from './Login.module.scss';
import translate from "../../i18n/translate";
import { MainLayout } from "../Layouts/MainLayout/MainLayout";
import { PrimaryButton } from "../buttons/PrimaryButton";
import classNames from "classnames";
import { PrimaryInput } from "../inputs/PrimatyInput/PrimaryInput";
import GoogleAuthButton from "../common/GoogleAuthButton";

const Login = () => {
    const { actions: authActions, state: authState } = useContext(AuthContext);
    const history = useHistory();
    const email = useInput('');
    const pass = useInput('');
    useEffect(() => {
        if (authState.loggedIn) {
            history.push('/')
        }
    }, [authState.loggedIn])
    const send = () => {
        if (!pass.value.length || !email.value.length) {
            return alert('Заполните все поля');
        }

        authActions.login(email.value, pass.value)
    }

    return <MainLayout>
        <div>
            <div className={styles.formWrapper}>
                <div className={styles.titleText}>{translate(MESSAGES_KEYS.login)}</div>
                <PrimaryInput type={'text'} {...email} translatedPlaceholder={MESSAGES_KEYS.email} />
                <PrimaryInput type={'password'} {...pass} translatedPlaceholder={MESSAGES_KEYS.password} />
                <div className={styles.btnWrapper}>
                    <PrimaryButton className={styles.btn} onClick={send}>{translate(MESSAGES_KEYS.login)}</PrimaryButton>
                    <div className={styles.orText}>Или</div>
                    <GoogleAuthButton />
                </div>
            </div>
            <div className={styles.fromFooter}>
                <div className={styles.fromFooterItem}>
                    <a href="/registration">Не помню пароль</a>
                </div>
                <div className={styles.fromFooterItem}>
                    <div>Нет аккаунта?</div> <a href="/registration">Регистрация</a>
                </div>
            </div>
        </div>
    </MainLayout>
}

export default Login;