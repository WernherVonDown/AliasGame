import React, { Fragment } from 'react';
import { IntlProvider } from 'react-intl';
import LOCALES from '../const/i18n/LOCALES';
import messages from './messages';

interface IProps {
    locale: string;
    children: React.ReactNode
}

const Provider: React.FC<IProps> = ({ children, locale = LOCALES.ENGLISH }) => (
    <IntlProvider
        locale={locale}
        textComponent={Fragment}
        messages={messages[locale]}
    >
        {children}
    </IntlProvider>
)

export default Provider;