import LOCALES from "../../const/i18n/LOCALES";
import MESSAGES_KEYS from "./MESSAGES_KEYS";

export default {
    [LOCALES.RUSSIAN]: {
        [MESSAGES_KEYS.hello]: 'привет',
        [MESSAGES_KEYS.login]: 'Войти',
        [MESSAGES_KEYS.password]: 'Пароль',
        [MESSAGES_KEYS.register]: 'Регистрация',
        [MESSAGES_KEYS.toMainPage]: 'На главную',
        [MESSAGES_KEYS.email]: 'Почта',
        [MESSAGES_KEYS.logout]: 'Выйти',
        [MESSAGES_KEYS.name]: 'Имя',
        [MESSAGES_KEYS.repeat_password]: 'Повторите пароль',
        [MESSAGES_KEYS.main_page_slider_text_1_title]: 'Добро пожаловать на AliasGame.ru!',
        [MESSAGES_KEYS.main_page_slider_text_1_description]: 'Сайт для практики иностранного языка и игры с друзьями.',
        [MESSAGES_KEYS.main_page_slider_text_2_title]: 'Объясняй на иностранном языке слова своим друзьям!',
        [MESSAGES_KEYS.main_page_slider_text_2_description]: 'Разговорная практика помогает быстрее заговорить на изучаемом языке.',
        [MESSAGES_KEYS.main_page_slider_text_3_title]: 'Сам слушай иностранную речь и угадывай слова',
        [MESSAGES_KEYS.main_page_slider_text_3_description]: 'Это помогает развить понимание речи на слух в игровой форме.',
        [MESSAGES_KEYS.main_page_slider_text_4_title]: 'Начни тренировать практические навыки иностранного языка на AliasGame.ru!',
        [MESSAGES_KEYS.main_page_slider_text_4_description]: 'Это также способ хорошо провести время с друзьями.',
        [MESSAGES_KEYS.main_page_onboarding_text]: 'Всем известная игра Alias как ни одна другая подходит для развития разговорных навыков. Самое сложное в изучении иностранного языка - это начать говорить. Даже имея возможность практиковаться, например, с другими людьми, изучающими язык, возникают трудности в выборе темы, из-за этого такое общение очень сковывает, выглядит неловко и в конце концов ни к чему не приводит. Игра Alias из-за специфики подходит для любых уровней владения языком. Можно пытаться объяснить слово даже на пальцах, т.к. в игре присутсвует видеочат. Игравая составляющая и игра в команде позволяет раскрепоститься и в веселой обставке начать применять теоретические знания, приобретенные при изучении языка.',
        [MESSAGES_KEYS.choose_language_pair]: 'Выбери языки',
        [MESSAGES_KEYS.use_custom_words]: 'Использовать свои слова',
        [MESSAGES_KEYS.enter]: 'Погнали!',
        [MESSAGES_KEYS.enter_to_add_custom_lists]: 'Войдите, что иметь возможно добавлять свои списки',
        [MESSAGES_KEYS.your_name]: 'Ваше имя',
        [MESSAGES_KEYS.send]: 'Отправить',
        [MESSAGES_KEYS.start_game]: 'Начать игру',
        [MESSAGES_KEYS.enter_your_message]: 'Введите сообщение',
        [MESSAGES_KEYS.are_you_ready]: 'Готовы?',
        [MESSAGES_KEYS.guessed]: 'Угадал!',
        [MESSAGES_KEYS.waiting_user_start_game]: 'Ждём пока пользователь начнет игру...',
        [MESSAGES_KEYS.waiting_user_user_ready]: 'Ждём пока пользователь будет готов...',
        [MESSAGES_KEYS.russian]: 'Русский',
        [MESSAGES_KEYS.french]: 'Французский',
        [MESSAGES_KEYS.english]: 'Английский',
        [MESSAGES_KEYS.german]: 'Немецкий',
        [MESSAGES_KEYS.no_message_yet]: 'Пока нет сообщений',
        [MESSAGES_KEYS.rules]: 'Правила',
        [MESSAGES_KEYS.rules_text_title]: 'Правила игры в Alias',
        [MESSAGES_KEYS.rules_text_title2]: 'Разговаривать можно только на выбранном иностранном языке.',
        [MESSAGES_KEYS.rules_text]: `1. Участники деляться на команды разных цветов по два человека.\n
        2. Каждый раунд ходит следующуя команда.\n
        3. В команде поочередно меняются роли того, кто угадывает и кто объясняет\n
        4. Необходимо за ограниченной вермя угадать как можно больше слов.\n
        5. Тот, кто в команде объясняет должен говорить ТОЛЬКО НА ИНСОТРАННОМ ЯЗЫКЕ.\n
        6. Тот, кто угадывает, может угадывать на родном языке, но лучше на иностранном\n
        7. Побеждает та команда, которая наберет больше всех очков.\n`,
    }
}