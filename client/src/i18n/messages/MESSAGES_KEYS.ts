import LANGUAGE_KEYS from "./LANGUAGE_KEYS";

export default {
    ...LANGUAGE_KEYS,
    hello: 'hello',
    login: 'login',
    register: 'register',
    password: 'password',
    email: 'email',
    toMainPage: 'Main page',
    logout: 'logout',
    name: 'name',
    repeat_password: 'repeat password',
    main_page_slider_text_1_title: 'main_page_slider_text_1_title',
    main_page_slider_text_1_description: 'main_page_slider_text_1_description',
    main_page_slider_text_2_title: 'main_page_slider_text_2_title',
    main_page_slider_text_2_description: 'main_page_slider_text_2_description',
    main_page_slider_text_3_title: 'main_page_slider_text_3_title',
    main_page_slider_text_3_description: 'main_page_slider_text_3_description',
    main_page_slider_text_4_title: 'main_page_slider_text_4_title',
    main_page_slider_text_4_description: 'main_page_slider_text_4_description',
    main_page_onboarding_text: 'main_page_onboarding_text',
    choose_language_pair: 'choose_language_pair',
    use_custom_words: 'use_custom_words',
    enter: 'enter',
    enter_to_add_custom_lists: 'enter_to_add_custom_lists',
    your_name: 'your_name',
    send: 'send',
    start_game: 'start_game',
    enter_your_message: 'enter_your_message',
    are_you_ready: 'are_you_ready',
    guessed: 'guessed',
    waiting_user_start_game: 'waiting_user_start_game',
    waiting_user_user_ready: 'waiting_user_user_ready',
    no_message_yet: 'no_messages_yet',
    rules: 'rules',
    rules_text_title: 'rules_text_title',
    rules_text_title2: 'rules_text_title2',
    rules_text: 'rules_text',
}