import LOCALES from "../../const/i18n/LOCALES";
import MESSAGES_KEYS from "./MESSAGES_KEYS";

export default {
    [LOCALES.ENGLISH]: {
        [MESSAGES_KEYS.hello]: 'Hello',
        [MESSAGES_KEYS.login]: 'Login',
        [MESSAGES_KEYS.password]: 'Password',
        [MESSAGES_KEYS.register]: 'Register',
        [MESSAGES_KEYS.toMainPage]: 'Main page',
        [MESSAGES_KEYS.email]: 'Email',
        [MESSAGES_KEYS.logout]: 'Logout',
        [MESSAGES_KEYS.name]: 'Name',
        [MESSAGES_KEYS.password]: 'Password',
        [MESSAGES_KEYS.repeat_password]: 'Repeat password',
        [MESSAGES_KEYS.main_page_slider_text_1_title]: 'Welcome to AliasGame.com!',
        [MESSAGES_KEYS.main_page_slider_text_1_description]: 'A site for practicing a foreign language and playing with friends.',
        [MESSAGES_KEYS.main_page_slider_text_2_title]: 'Explain words in a foreign language to your friends!',
        [MESSAGES_KEYS.main_page_slider_text_2_description]: 'Conversational practice helps to speak faster in the target language.',
        [MESSAGES_KEYS.main_page_slider_text_3_title]: 'Listen to foreign speech yourself and guess the words',
        [MESSAGES_KEYS.main_page_slider_text_3_description]: 'It helps to develop listening comprehension in a playful way.',
        [MESSAGES_KEYS.main_page_slider_text_4_title]: 'Start training your practical foreign language skills at AliasGame.ru!',
        [MESSAGES_KEYS.main_page_slider_text_4_description]: 'It is also a way to have a good time with friends.',
        [MESSAGES_KEYS.main_page_onboarding_text]: "The well-known game Alias is more than any other suitable for the development of conversational skills. The most difficult thing in learning a foreign language is to start speaking. Even having the opportunity to practice, for example, with other language learners, there are difficulties in choosing a topic, because of this, such communication is very restrictive, looks awkward and in the end leads to nothing. The Alias ​​game is suitable for all levels of language proficiency due to its specificity. You can try to explain the word even on your fingers, because The game has video chat. Playing and playing in a team allows you to relax and start applying the theoretical knowledge acquired while learning the language in a fun environment.",
        [MESSAGES_KEYS.choose_language_pair]: 'Choose a language pair:',
        [MESSAGES_KEYS.use_custom_words]: 'Use custom words',
        [MESSAGES_KEYS.enter]: 'Enter',
        [MESSAGES_KEYS.enter_to_add_custom_lists]: 'Login to add your custom lists',
        [MESSAGES_KEYS.your_name]: 'Your name',
        [MESSAGES_KEYS.send]: 'Send',
        [MESSAGES_KEYS.start_game]: 'Start game',
        [MESSAGES_KEYS.enter_your_message]: 'Enter your message',
        [MESSAGES_KEYS.are_you_ready]: 'Are you ready?',
        [MESSAGES_KEYS.guessed]: 'Guessed!',
        [MESSAGES_KEYS.waiting_user_start_game]: 'Waiting for the user to start the game...',
        [MESSAGES_KEYS.waiting_user_user_ready]: 'Waiting for the user to be ready...',
        [MESSAGES_KEYS.russian]: 'Russian',
        [MESSAGES_KEYS.french]: 'French',
        [MESSAGES_KEYS.english]: 'English',
        [MESSAGES_KEYS.german]: 'German',
        [MESSAGES_KEYS.no_message_yet]: 'No messages yet',
        [MESSAGES_KEYS.rules]: 'Rules',
        [MESSAGES_KEYS.rules_text_title]: 'Alias game rules',
        [MESSAGES_KEYS.rules_text_title2]: 'You can only speak in the chosen foreign language.',
        [MESSAGES_KEYS.rules_text]: `1. Participants are divided into teams of different colors for two people.\n
        2. Each round, the next team goes.\n
        3. In the team, the roles of the one who guesses and who explains are alternately changed\n
        4. It is necessary to guess as many words as possible within a limited time.\n
        5. The person explaining on the team should ONLY speak IN A FOREIGN LANGUAGE.\n
        6. The one who guesses can guess in their native language, but better in a foreign\n
        7. The team with the most points wins.\n`,
    }
}