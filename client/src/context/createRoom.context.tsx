import React, { ReactElement, useState, useContext, useEffect } from "react";
import { Langs } from "../const/languages/LANGS";

interface IState {
    foreignLang: string;
    motherLang: string;

}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const CreateGameContext = React.createContext({} as ContextValue);

const CreateGameContextProvider = (props: IProps) => {
    const { children, state: defaultState } = props;
    const [state, setState] = useState<IState>(defaultState);

    const setForeign = (foreignLang: string) => {
        console.log('SET FOR', foreignLang)
        setState((p: IState) => (
            {
                ...p,
                foreignLang
            }
        ))
    }

    const startGame = () => {
        console.log('START', state)
    }

    const actions = {
        setForeign,
        startGame
    }

    return <CreateGameContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </CreateGameContext.Provider>
}

CreateGameContextProvider.defaultProps = {
    state: {
        foreignLang: Langs.EN,
        motherLang: Langs.RUS
    }
}

export { CreateGameContext, CreateGameContextProvider };