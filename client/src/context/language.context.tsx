import React, { ReactElement, useState, useContext, useEffect } from "react";
import axios from 'axios';
import { HTTP_EVENTS } from "../const/https/HTTP_EVENTS";
import { BACKEND_URL } from "../config/default";
import path from 'path';
import { UserContext } from "./user.context";
import LOCALES from "../const/i18n/LOCALES";
import { I18nProvider } from "../i18n";

interface IState {
    currentLanguage: string;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

axios.defaults.baseURL = BACKEND_URL

const LanguageContext = React.createContext({} as ContextValue);

const LanguageContextProvider = (props: IProps) => {
    const { children, state: defaultState } = props;
    const [state, setState] = useState<IState>(defaultState);

    useEffect(() => {
        setDefaultCurrentLanguge()
    }, [window.navigator.language]);

    const setDefaultCurrentLanguge = () => {
        if (window.navigator.language && Object.values(LOCALES).includes(window.navigator.language)) {
            setState((p: IState) => ({
                ...p,
                currentLanguage: window.navigator.language
            }))
        } else {
            setState((p: IState) => ({
                ...p,
                currentLanguage: LOCALES.ENGLISH
            }))
        }
    }

    const changeLanguage = (language: string) => {
        setState((p: IState) => (
            {
                ...p,
                currentLanguage: language,
            }
        ))
    }

    const actions = {
        changeLanguage,
    }

    return <LanguageContext.Provider
        value={{
            state,
            actions
        }}
    >
        <I18nProvider locale={state.currentLanguage}>
            {children}
        </I18nProvider>

    </LanguageContext.Provider>
}

LanguageContextProvider.defaultProps = {
    state: {
        defaultLanguge: LOCALES.ENGLISH
    }
}

export { LanguageContext, LanguageContextProvider };