import React, { useContext, useEffect } from 'react';
import { useSockets } from './context/socket.context';
// import 'materialize-css/dist/css/materialize.min.css'
import Users from './components/Users/Users';
import './index.scss'
import Game from './components/Game/Game';
import GameHistory from './components/GameHistory/GameHistory';
import styles from './app.module.scss'
import Chat from './components/Chat/Chat';
import CreateGame from './components/CreateGame/CreateGame';
import { Switch, Route } from 'react-router-dom'
import Room from './components/Room/Room';
import { VideoChatContextProvider } from './context/videoChat.context';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import AddCustomWords from './components/AddCustomWords/AddCustomWords';
import { LanguageContextProvider } from './context/language.context';
import { CreateGameContextProvider } from './context/createRoom.context';
import Donate from './components/Donate/Donate';
import { Landing } from './components/Landing/Landing';
import { AnimatePresence } from 'framer-motion'

const App = () => {
  // const { socket } = useSockets();

  useEffect(() => {
    // socket.emit('hello', 123)
  })

  return <AnimatePresence exitBeforeEnter><Switch>
    <LanguageContextProvider>
      <CreateGameContextProvider>
        <Route exact path='/' component={Landing} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/registration' component={Registration} />
        <Route exact path='/words/add' component={AddCustomWords} />
        <Route path='/room/:roomId' component={Room} />
        <Route exact path='/donate' component={Donate} />
      </CreateGameContextProvider>
    </LanguageContextProvider>
  </Switch>
  </AnimatePresence>

  // return (
  //   <div className={styles.content}>
  //     <CreateGame />
  //   </div>
  // );

  return (
    <div className={styles.content}>
      <Users />
      <div className={styles.mainPanelWrapper}>
        <GameHistory />
        <div className={styles.centerPanelWrapper}>
          <Game />
          <Chat />
        </div>
      </div>
    </div>
  );
}

export default App;
